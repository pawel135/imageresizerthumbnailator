# Converter to about 150x100 pixels (keeping aspect ratio)

## How it works

Files in resources/faces/ are converted and saved to resources/converted/.
Conversion also includes rotation of 90 degrees clockwise if image_width > image_height.
This doesn't always work properly, so manual inspection of results is recommended.
It is vital that every group of images has its own parent directory inside resources/faces/ directory. This parent directory is also used to name output file as follows:

resources/faces/PerSon/some_image.jpg -> resources/converted/PerSon/person1.jpg

each subsequent file gets bigger number. Files already created in the output firectory and with the same name are overwritten.

## How to launch

Put your directories with people in /home/pawel13/Downloads/IntelliJ/IdeaProjects/ImageResizedThumbnailator/image_resizer_thumbnailator/src/main/resources/faces/
If necessary, change extension of files (".jpg, .png, etc.")
The result will be stored in /home/pawel13/Downloads/IntelliJ/IdeaProjects/ImageResizedThumbnailator/image_resizer_thumbnailator/src/main/resources/converted

#### Manually, using java compiler
while being in ~/Downloads/IntelliJ/IdeaProjects/ImageResizedThumbnailator/image_resizer_thumbnailator/src/main/java
    exeute:
javac com/pawel13/resizer/App.java
java com/pawel13/resizer/App (full path from any location)


this is only possible because dependency was also downloaded and put in src/main/java/net directory

#### Automatically, using Maven with jar creation (and jar executable launch)
mvn clean install
java -jar target/image_resizer_thumbnailator-1.0-SNAPSHOT.jar

###### author: Paweł Szczepanowski
