package com.pawel13.resizer;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.filters.Flip;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    private static String FILE_EXTENSION = "png";
    private static String NEW_DIR_NAME = "converted";
    private static final boolean CONVERT_TO_GREYSCALE = true;
    private static final boolean KEEP_ASPECT_RATIO = true;

    private static final int HOW_MANY_ROTATIONS = 0;
    private static final int MAX_ANGLE = 20; //in degrees
    private static final Random RANDOM = new Random(123);

    private static final int HOW_MANY_FLIPS = 0; //0-3, in sequence: no flip, vertical, horizontal, both
    private static final List<Function<Thumbnails.Builder, Thumbnails.Builder>> FLIPS = Arrays.asList(
            b -> b,
            b -> b.addFilter(Flip.HORIZONTAL),
            b -> b.addFilter(Flip.VERTICAL),
            b -> b.addFilters(Arrays.asList(Flip.HORIZONTAL, Flip.VERTICAL))
    );
    private static final int MAX_WIDTH = 50;
    private static final int MAX_HEIGHT = 50;
    private static final String[] ADDONS = "abcdefghijklmnopqrstuvwxyz".split("(?<=\\G.{1})");

    public static void main(String[] args) {
        try (Stream<Path> paths = Files.walk(Paths.get("src/main/resources/faces/"))) {
            List<Path> imagePaths = paths.filter(Files::isRegularFile)
                    .filter(p -> p.toString().matches(".*\\." + Pattern.quote(FILE_EXTENSION)))
                    .collect(Collectors.toList());
            int i = 1;
            String dirName = "";
            String oldDirName;
            Path upUpUpDirPath;
            for (Path imagePath : imagePaths) {

                try {
                    oldDirName = dirName;
                    dirName = imagePath.toFile().getParentFile().getName(); // only parent dir name
                    if (i != 1 && !dirName.equals(oldDirName)) {
                        i = 1;
                    }
                    //System.out.println("parent dir name: " + dirName);
                    //System.out.println(imagePath.getParent().toString()); //path to parent dir

                    //String outputFilename = dirName.toLowerCase()+ i + "." + FILE_EXTENSION;
                    String outputFilenameBaseNoExtension = dirName.toLowerCase() + i;
                    //System.out.println("result filename: " + outputFilenameBaseNoExtension);

                    upUpUpDirPath = imagePath.getParent().getParent().getParent();
                    //					resultPath = Paths.get(upUpUpDirPath.toString(), NEW_DIR_NAME, dirName, outputFilename);
                    final Path resultPathBase = Paths.get(upUpUpDirPath.toString(), NEW_DIR_NAME, dirName, outputFilenameBaseNoExtension);
                    //TODO: remove folder content              resultPathBase.
                    //System.out.println( "combined path: " + resultPathBase.toString() );

                    if (i == 1) {
                        Files.createDirectories(resultPathBase.getParent());
                    }
		/*  Also OK
							BufferedImage bi = Thumbnails.of( ImageIO.read(imagePath.toFile()) )
								.size(150,100)
		//						.scale(0.25)
								.asBufferedImage();
							ImageIO.write(bi, "jpg", resultPath.toFile());
		*/

                    //					BufferedImage inputImage = ImageIO.read(imagePath.toFile());
                    BufferedImage inputImageColor = ImageIO.read(imagePath.toFile());

                    //	convert to greyscale
                    BufferedImage inputImage = CONVERT_TO_GREYSCALE ?
                            new BufferedImage(inputImageColor.getWidth(), inputImageColor.getHeight(), BufferedImage.TYPE_BYTE_GRAY)
                            : inputImageColor;
                    Graphics g = inputImage.getGraphics();
                    g.drawImage(inputImageColor, 0, 0, null);
                    g.dispose();


                    //TODO: is it really ThumbnailMaker?
                    Thumbnails.Builder<BufferedImage> imageBaseBuilder = Thumbnails.of(inputImage)
                            .size(MAX_WIDTH, MAX_HEIGHT)
                            .keepAspectRatio(KEEP_ASPECT_RATIO);
//								.rotate( inputImage.getWidth() > inputImage.getHeight() ? 90 : 0 ); //clockwise

                    //original image with minor changes
		/*					Thumbnails.of( inputImage )
								.size(MAX_WIDTH,MAX_HEIGHT)
								.rotate( inputImage.getWidth() > inputImage.getHeight() ? 90 : 0 ) //clockwise
		//						.scale(0.25)
								.toFile(Paths.get(resultPathBase.toString()+"."+FILE_EXTENSION));
		*/
//or even better
                    //Thumbnails.of(cloneBufferedImage(imageBaseBuilder.asBufferedImage())).toFile(resultPath.toFile());

                    List<Path> filePaths = Arrays.stream(ADDONS)
                            .map(s -> resultPathBase.toString() + s + "." + FILE_EXTENSION)
                            .map(Paths::get)
                            .collect(Collectors.toList());
                    makeTransformationsAndSave(imageBaseBuilder, filePaths, resultPathBase);

                } catch (IOException e2) {
                    System.err.println("IO Exception");
                }

                i++;
            }
        } catch (IOException e) {
            System.err.println("Cannot walk through specified path. Probably path is incorrectly specified.");
        }

    }

    private static final void makeTransformationsAndSave(Thumbnails.Builder baseBuilder, List<Path> pathNames, Path resultPathBase) throws IOException {
        if (baseBuilder == null || pathNames == null || pathNames.size() < 1) {
            throw new IllegalArgumentException();
        }
        final String DEFAULT_NAME = pathNames.get(0).toString();

        Iterator<Path> pathNameIterator = pathNames.iterator();
        //Thumbnails.of(cloneBufferedImage(baseBuilder.asBufferedImage())).scale(1).toFile(Paths.get(DEFAULT_NAME).toFile());

        Thumbnails.of(cloneBufferedImage(baseBuilder.asBufferedImage()))
                .scale(1)
                .toFile(resultPathBase.toString() + "." + FILE_EXTENSION);

        for (int i = 1; i <= HOW_MANY_ROTATIONS; i++) {
            int randomAngle = i == 0 ? 0 : RANDOM.nextInt(MAX_ANGLE * 2 + 1) - MAX_ANGLE;
            Thumbnails.of(cloneBufferedImage(baseBuilder.asBufferedImage()))
                    .scale(1)
                    .rotate(randomAngle)
                    .toFile(pathNameIterator.hasNext() ? pathNameIterator.next().toFile() : Paths.get(DEFAULT_NAME + "_rotation_" + i).toFile());
        }
        for (int i = 1; i <= HOW_MANY_FLIPS; i++) {
            Thumbnails.Builder result = Thumbnails.of(cloneBufferedImage(baseBuilder.asBufferedImage()))
                    .scale(1);
            FLIPS.get(i).apply(result)
                    .toFile(pathNameIterator.hasNext() ? pathNameIterator.next().toFile() : Paths.get(DEFAULT_NAME +"_flip_"+ i).toFile());
        }

    }

    private static final BufferedImage cloneBufferedImage(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

}

